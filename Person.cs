using System;

namespace c_g1s1
{   
    public enum Sex {
        Male = 1,
        Female = 2,
        Undefined = 3
    }
    public class Person {

        private string userId;
        private string firstName{get; set; }
        private string lastName{get; set; }
        private string gender{get; set; }
        private DateTime birthDate{get;set;}

        public Person (string userId) {
            this.userId = userId;
        }

        public Person(string userId, string firstName, string lastName):this(userId) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public Person(string userId, string firstName, string lastName, int year, int month, int day):this(userId, firstName, lastName) {
            birthDate = new DateTime(year, month, day);
        }

        public string getFullName() {
            return $"{firstName} {lastName}";
        }

        public double GetAge() {
            double age = (DateTime.Now - birthDate).TotalDays;
            return Math.Round((age/365));
        }
    }
}